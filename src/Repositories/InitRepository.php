<?php

namespace SpondonIt\IdeskService\Repositories;

use App\Models\User;
use App\Models\HdTicket;
use App\Models\HdStaticPage;
use App\Models\HdNotification;
use App\Models\HdTicketFolder;
use App\Models\HdEnvatoCategory;
use App\Models\HdTicketFeedback;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;
use Modules\Language\Entities\Language;
use Modules\RolePermission\Entities\Role;
use Modules\GeneralSetting\Entities\GeneralSetting;

class InitRepository
{

    public function init()
    {
        config([
            'app.item' => '34962179',
            'spondonit.module_manager_model' => \Modules\ModuleManager\Entities\InfixModuleManager::class,
            'spondonit.module_manager_table' => 'infix_module_managers',

            'spondonit.settings_model' => \Modules\GeneralSetting\Entities\GeneralSetting::class,
            'spondonit.module_model' => \Nwidart\Modules\Facades\Module::class,

            'spondonit.user_model' => \App\Models\User::class,
            'spondonit.settings_table' => 'general_settings',
            'spondonit.database_file' => '',
            'spondonit.php_version' => 8.1
        ]);
    }


    public function config()
    {
        if(Schema::hasTable('general_settings')){
            $dataGeneral = GeneralSetting::first();
        }else{
            $dataGeneral = null;
        }

        if (config('app.force_https')) {
            URL::forceScheme('https');
        }

        view()->composer('frontend.default.welcome', function ($view) {
            $headerData =[
                'allCustomerCount' => User::where('role_id', 4)->count(),
                'feedbackCount' => HdTicketFeedback::count(),
                'solveTicketCount' => HdTicket::where('last_status' ,'Closed')->count(),
                'agentCount' => User::where('role_id' , 4)->count()
            ];
            $view->with($headerData);
        });

        view()->composer('backEnd.partials._ticketMenu', function ($view) {
            $notifications =['notifications' => HdNotification::where('user_id', auth()->user()->id)->orderBy('id', 'desc')->get()];
            $view->with($notifications);
        });

        view()->composer('backEnd.partials._ticketSidebar', function ($userWiseFolder) {
            $allFolders = [
                'allFolders' => HdTicketFolder::with('folderDetails')->where('user_id', auth()->user()->id)->get(),
                'allStatus' => HdEnvatoCategory::with('ticketStatus')->where('type', 'status')->get(),
                'notReplied' => HdTicket::with('envatoCategory')
                            ->whereHas('envatoCategory', function ($ec) {
                                $ec->whereIn('type', allTypeOfProducts());
                            })
                            ->when(auth()->user()->role->type == "staff", function ($s) {
                                $s->whereJsonContains('assign_to', (string) auth()->user()->id);
                            })
                            ->when(auth()->user()->role->type == "customer", function ($c) {
                                $c->where('created_by', auth()->user()->id)->where('client_reply', 0);
                            })
                            ->when(auth()->user()->role->type != "customer", function ($a) {
                                $a->where('client_reply', 1);
                            })
                            ->where('last_status', '!=' ,'Closed')
                            ->get(),
                'allTicket' => HdTicket::with('envatoCategory')
                            ->whereHas('envatoCategory', function ($ec) {
                                $ec->whereIn('type', allTypeOfProducts());
                            })
                            ->when(auth()->user()->role->type == "staff", function ($p) {
                                $p->whereJsonContains('assign_to', (string) auth()->user()->id);
                            })
                            ->when(auth()->user()->role->type == "customer", function ($p) {
                                $p->where('created_by', auth()->user()->id);
                            })
                            ->get(),
                'permissionAllTicket' => HdTicket::with('envatoCategory')
                            ->whereHas('envatoCategory', function ($ec) {
                                $ec->whereIn('type', allTypeOfProducts());
                            })->count(),
                'notRepliedPermission' => HdTicket::with('envatoCategory')
                            ->whereHas('envatoCategory', function ($ec) {
                                $ec->whereIn('type', allTypeOfProducts());
                            })
                            ->when(auth()->user()->role->type != "customer", function ($a) {
                                $a->where('client_reply', 1);
                            })
                            ->where('last_status', '!=' ,'Closed')
                            ->get(),
                'mineTicket' => HdTicket::with('envatoCategory')
                            ->whereHas('envatoCategory', function ($ec) {
                                $ec->whereIn('type', allTypeOfProducts());
                            })
                            ->whereJsonContains('assign_to', (string) auth()->user()->id)
                            ->where('last_status', '!=' ,'Closed')
                            ->count(),

                'mineNotReplied' => HdTicket::with('envatoCategory')
                            ->whereHas('envatoCategory', function ($ec) {
                                $ec->whereIn('type', allTypeOfProducts());
                            })
                            ->whereJsonContains('assign_to', (string) auth()->user()->id)
                            ->where('client_reply', 1)
                            ->where('last_status', '!=' ,'Closed')
                            ->count(),
                            
                'envatoCategories' => HdEnvatoCategory::whereIn('type', allTypeOfProducts())->with('envatoCategories', 'ticketInfos')->get(),
                'unassignedTicket' => HdTicket::where('last_status', '!=', 'Closed')->whereNull('assign_to')->count(),
            ];
            $userWiseFolder->with($allFolders);
        });

        if(Schema::hasTable('general_settings')){
            app()->singleton('general_setting', function () use($dataGeneral) {
                return $dataGeneral;
            });
        }

        app()->singleton('permission_list', function() {
            return Role::with(['permissions' => function($query){
                $query->select('route','module_id','parent_id','role_permission.role_id');
            }])->get(['id','name']);
        });

        app()->singleton('langs', function(){
            return Language::where('status',1)->get();
        });

        app()->singleton('current_lang', function() use($dataGeneral){
            if(auth()->check()){
                return Language::where('code', auth()->user()->lang_code)->select('rtl')->first();
            }elseif (Session::get('locale')) {
                $locale = Session::get('locale');
                return Language::where('code', $locale)->select('rtl')->first();
            }else{
                return Language::where('code', @$dataGeneral->language_code)->select('rtl')->first();
            }
        });
    }
}
