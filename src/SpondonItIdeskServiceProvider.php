<?php

namespace SpondonIt\IdeskService;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Http\Kernel;
use Illuminate\Support\Facades\Storage;
use SpondonIt\IdeskService\Middleware\IdeskService;
use Modules\ModuleManager\Entities\InfixModuleManager;

class SpondonItIdeskServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $kernel = $this->app->make(Kernel::class);
        $kernel->pushMiddleware(IdeskService::class);

        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'idesk');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'idesk');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        if(Storage::exists('.app_installed') && Storage::get('.app_installed')){
            app()->singleton('ModuleList', function() {
                return InfixModuleManager::all();
            });
        }
    }
}
