<?php

use Carbon\Carbon;
use App\Models\HdTicketAvailability;

if (!function_exists('isModuleActive')) {
    function isModuleActive($module)
    {
        try {
            $haveModule = app('ModuleList')->where('name', $module)->first();
            $is_module_available = 'Modules/' . $module . '/Providers/' . $module . 'ServiceProvider.php';
            if (file_exists($is_module_available)) {
                $moduleCheck = \Nwidart\Modules\Facades\Module::find($module)->isEnabled();
                if (!$moduleCheck) {
                    return false;
                }
                if ($haveModule) {
                    if (!empty($haveModule->purchase_code)) {
                        return true;
                    }
                }else{
                    return $moduleCheck;
                }
            }
            return false;
        } catch (\Throwable $th) {
            return false;
        }
    }
}


if (! function_exists('permissionCheck')) {
    function permissionCheck($route_name)
    {
        if(auth()->check()){
            if(auth()->user()->role->type == "superadmin"){
                return TRUE;
            }elseif (auth()->user()->role->type == "custom") {
                if(auth()->user()->permissions->contains('route',$route_name)){
                    return TRUE;
                }else{
                    return FALSE;
                }
            }else{
                $roles = app('permission_list');
                $role = $roles->where('id',auth()->user()->role_id)->first();
                if($role != null && $role->permissions->contains('route',$route_name)){
                    return TRUE;
                }else{
                    return FALSE;
                }
            }
        }
        return FALSE ;
    }
}

if (!function_exists('envu')) {
    function envu($data = array())
    {
        foreach ($data as $key => $value) {
            if (env($key) === $value) {
                unset($data[$key]);
            }
        }

        if (!count($data)) {
            return false;
        }

        $env = file_get_contents(base_path() . '/.env');
        $env = explode("\n", $env);
        foreach ((array) $data as $key => $value) {
            foreach ($env as $env_key => $env_value) {
                $entry = explode("=", $env_value, 2);
                if ($entry[0] === $key) {
                    $env[$env_key] = $key . "=" . (is_string($value) ? '"' . $value . '"' : $value);
                } else {
                    $env[$env_key] = $env_value;
                }
            }
        }
        $env = implode("\n", $env);
        file_put_contents(base_path() . '/.env', $env);
        return true;
    }
}

if (!function_exists('findFirstParent')) {
    function findFirstParent($category)
    {
        if($category->parent){
            return findFirstParent($category->parent);
        }
        return $category;
    }
}

if (!function_exists('parentIds')) {
    function parentIds($parent, $ids = [])
    {
        if($parent->parent){
            $ids[] = $parent->parent->id;
            return parentIds($parent->parent, $ids);
        } 
        return $ids;
    }
}

if (!function_exists('parentSlug')) {
    function parentSlug($parent, $slugs = [])
    {
        if($parent->parent){
            $slugs[] = $parent->parent->slug;
            return parentSlug($parent->parent, $slugs);
        } 
        return $slugs;
    }
}

if (!function_exists('dynamicUrl')) {
    function dynamicUrl($slugArray, $catSlug)
    {
        return implode('/', array_reverse($slugArray)).'/'.$catSlug;
    }
}

if (!function_exists('ticketAvailability')) {
    function ticketAvailability()
    {
        $currentDayData = Carbon::now();
        $currentTime = $currentDayData->format('H:i:s');
        $currentDay = $currentDayData->format('l');

        $ticketAvailableData = HdTicketAvailability::where('day', $currentDay)
                ->where('start', '<=', $currentTime)
                ->where('end', '>=', $currentTime)
                ->first();

        $startAndEndTime = HdTicketAvailability::where('day', $currentDay)->first();
        if($ticketAvailableData){
            $noteData = __('dashboard.working_hours').': '. Carbon::parse(@$startAndEndTime->start)->format('g:i A') ." ". __('common.to') ." ". Carbon::parse($startAndEndTime->end)->format('g:i A');
            $data['status'] = true;
            $data['note'] = $noteData;
            return $data;
        }else{
            $data['status'] = false;
            if(is_null($startAndEndTime)){
                $data['note'] = __('dashboard.today_is_weekend');
            }else{
                $noteData = __('dashboard.working_hours').': '. Carbon::parse(@$startAndEndTime->start)->format('g:i A') ." ". __('common.to') ." ". Carbon::parse($startAndEndTime->end)->format('g:i A');
                $data['note'] = $noteData;
            }
            return $data;
        }
    }
}


if (!function_exists('isRtl')) {
    function isRtl()
    {
        if (app()->bound('current_lang') && app('current_lang')->rtl == 1) {
            return true;
        }
        return false;
    }
}